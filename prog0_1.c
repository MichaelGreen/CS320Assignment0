#include <stdio.h>

int main(void){
	char nombre[256];
	printf("Assignment #0-1, Michael, Green\nWhat is your name?\n");
	/* So we have this issue where we don't know the size of the user's
	name which leaves us with some options for implementation:
		1.) We could just pick an arbitrary/reasonable size for an English name,
		allocate that space in memory read in the string and hope it fits.
		2.) We could implement #1 but read in char by char and reallocate the the space
		every time it exeeds the current size.
		3.) We could use some sort of linked-list structure where each character node
		references the next and when we are finished reading we can go back to the head
		and traverse the list outputing each character as we go along
		4.) We could combine #2 and #3 and find some sort of balance between the cost
		of storing references and the cost of reallocating memory.
	but who cares.
	*/
	scanf("%[^\n]%*c",nombre);
	printf("Hello %s!",nombre);
	return 0;
}
